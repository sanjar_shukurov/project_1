from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Sum
from .models import *
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView,
    View,
    TemplateView,
    DetailView,

)
from django.http import HttpResponse


from .models import *
# Create your views here.


class WarehouseView(ListView):
    model = Warehouse
    context_object_name = 'warehouses'
    template_name = "keeper/warehouse.html"


class AgentView(ListView):
    model = CounterParty
    context_object_name = 'agents'
    template_name = "keeper/agent.html"


class WorkerView(ListView):
    model = Workers
    context_object_name = 'workers'
    template_name = "keeper/worker.html"


class ProductView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = "keeper/products.html"

    def cart(self, request, pk):
        item = Product.objects.get(pk=pk)


class WarProductsView(ListView):
    model = WarehouseProduct
    context_object_name = 'prods'
    template_name = "keeper/w_products.html"


    def get_queryset(self):
        current_id = self.kwargs.get('pk', None)
        queryset = WarehouseProduct.objects.filter(warehouse_id=current_id)
        return queryset





class WarProdAddView(CreateView):
    model = WarehouseProduct
    template_name = 'keeper/warehouseproduct_form.html'
    fields = ['product', 'price', 'count', 'warehouse']




class ProdAddView(CreateView):
    model = Product
    template_name = 'keeper/product_form.html'
    fields = ['title', 'selling_price', 'category']

class ProdDetailView(DetailView):
    model = WarehouseProduct
    template_name = 'keeper/warehouse_detail.html'

class CounterPartyView(ListView):
    model = CounterParty
    template_name = 'keeper/counterparty.html'
    context_object_name = 'counterparties'

class CounterPartyDetail(DetailView):
    model = CounterParty
    template_name = 'keeper/counterparty_detail.html'

class CounterPartyCreateView(CreateView):
    model = CounterParty
    template_name = 'keeper/counterparty_form.html'
    fields = ['name', 'balance', 'warehouse', 'org_name', 'phone', 'phone_2', 'address', 'inn', 'status', 'sms_live', 'extra_info']

class IncomeView(ListView):
    model = Income
    template_name = 'keeper/income.html'
    context_object_name = 'incomes'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        income_item = IncomeItem.objects.filter(income_id=current_id)
        total = income_item.count()
        context['totals'] = total
        return context



class IncomeCreateView(CreateView):
    model = Income
    template_name = 'keeper/income_form.html'
    fields = ['counter_party_id','warehouse', 'user' ]

    def get_success_url(self):
        return reverse("cp-detail", kwargs={'pk': self.kwargs['id']})



class IncomeDetailView(DetailView):
    model = Income
    template_name = 'keeper/income_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        income_item = IncomeItem.objects.filter(income_id=current_id)

        context['items'] = income_item
        context['current_id'] = current_id
        return context

class IncomeItemCreateView(CreateView):
    model = IncomeItem
    template_name = 'keeper/income_item_form.html'
    fields = ['product_id', 'count',  'price']

    def get_success_url(self):
        return reverse("income-detail", kwargs={'pk': self.kwargs['id']})

    def form_valid(self, form):
        form.instance.income_id_id = self.kwargs['id']
        form.instance.total = float(self.request.POST.get('price', None)) * \
                              float(self.request.POST.get('count', None))
        f = super().form_valid(form)
        income_items = IncomeItem.objects.filter(income_id_id=self.kwargs['id']).aggregate(Sum('total'))['total__sum']
        Income.objects.filter(id=self.kwargs['id']).update(total=income_items)
        return f

class ChangeStatusView(View):
    def get(self, request, *args, **kwargs):
        income_id = self.kwargs["id"]
        action = request.GET.get("action")
        income_obj = Income.objects.get(id=income_id)
        items = IncomeItem.objects.filter(income_id_id=self.kwargs['id'])
        if income_obj.status == "Open":
            if action == "next":
                income_obj.status = "Closed"
                income_obj.save()
                for item in items:
                    obj, created = WarehouseProduct.objects.get_or_create(
                        product=item.product_id,
                        defaults = {
                            'warehouse': income_obj.warehouse,
                            'price': item.price,
                            'count': item.count,
                        }
                    )
                    if not created:
                        obj.count += item.count
                        obj.save()

            elif action == "reject":
                income_obj.status = "Rejected"
                income_obj.save()
        elif income_obj.status == "Closed":
            if action == "prev":
                income_obj.status = "Open"
                income_obj.save()
            elif action == "next":
                income_obj.status = "Completed"
                income_obj.save()
        return redirect("income")


class OrderView(ListView):
    model = Order
    template_name = 'keeper/order.html'
    context_object_name = 'orders'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        order_item = OrderItem.objects.filter(order_id=current_id)
        total = order_item.count()
        context['totals'] = total
        return context

class OrderDetailView(DetailView):
    model = Order
    template_name = 'keeper/order_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        order_item = OrderItem.objects.filter(order_id=current_id)
        context['order_items'] = order_item
        context['current_id'] = current_id
        return context

class OrderCreateView(CreateView):
    model = Order
    template_name = 'keeper/order_form.html'
    fields = ['counter_party_id', 'user' ]

class OrderItemCreateView(CreateView):
    model = OrderItem
    template_name = 'keeper/order_item_form.html'
    fields = ['product_id', 'count']

    def get_success_url(self):
        return reverse("order-detail", kwargs={'pk': self.kwargs['id']})

    def form_valid(self, form):
        a = WarehouseProduct.objects.get(product=form.instance.product_id.product)
        form.instance.order_id_id = self.kwargs['id']
        form.instance.price = a.price
        form.instance.total = float(form.instance.price) * float(self.request.POST.get(
            "count", None
        ))
        form.instance.warehouse = a.warehouse
        f = super().form_valid(form)
        order_items = OrderItem.objects.filter(order_id_id=self.kwargs['id']).aggregate(Sum('total'))['total__sum']
        Order.objects.filter(id=self.kwargs['id']).update(total=order_items)
        return f

class ChangeOrderStatusView(View):
    def get(self, request, *args, **kwargs):
        order_id = self.kwargs["id"]
        action = request.GET.get("action")
        order_obj = Order.objects.get(id=order_id)
        order_items = OrderItem.objects.filter(order_id_id=self.kwargs['id'])

        if order_obj.status == "Open":
            if action == "next":
                order_obj.status = "Closed"
                order_obj.save()
                for order_item in order_items:
                    warehouse_product = WarehouseProduct.objects.get(id=order_item.product_id_id)
                    warehouse_product.count -= order_item.count
                    warehouse_product.save()
            elif action == "reject":
                order_obj.status = "Rejected"
                order_obj.save()
        elif order_obj.status == "Closed":
            if action == "prev":
                order_obj.status = "Open"
                order_obj.save()
            elif action == "next":
                order_obj.status = "Completed"
                order_obj.save()
        elif order_obj.status == "Completed":
            if action == "prev":
                order_obj.status = "Closed"
                order_obj.save()

        return redirect("order")


class PaymentView(ListView):
    model = OrderPaymentLog
    template_name = 'keeper/payment_log.html'
    context_object_name = 'payments'

class PaymentDetailView(DetailView):
    model = Order
    template_name = 'keeper/payment_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        order = OrderPaymentLog.objects.filter(order_id=current_id)
        context['order_items'] = order
        context['current_id'] = current_id
        return context


class PaymentCreateView(CreateView):
    model = OrderPaymentLog
    template_name = 'keeper/payment_form.html'
    fields = ['outcat', 'outlay', 'amount_type','comment']

    def get_success_url(self):
        return reverse("order-detail", kwargs={'pk': self.kwargs['id']})

    def form_valid(self, form):
        form.instance.order_id = self.kwargs['id']
        form.instance.amount = OrderItem.objects.filter(order_id=self.kwargs['id']).aggregate(Sum('total'))['total__sum']
        f = super().form_valid(form)
        return f


class IncPayDetailView(DetailView):
    model = Income
    template_name = 'keeper/inc_pay_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_id = self.kwargs.get('pk', None)
        income = IncomePaymentLog.objects.filter(outlay=current_id)
        context['income_items'] = income
        context['current_id'] = current_id
        return context


class IncPayCreateView(CreateView):
    model = IncomePaymentLog
    template_name = 'keeper/inc_pay_form.html'
    fields = [ 'amount_type', 'comment', 'payment_method']

    def get_success_url(self):
        return reverse("income-detail", kwargs={'pk': self.kwargs['id']})

    def form_valid(self, form):
        form.instance.outlay = Income.objects.get(id=self.kwargs['id'])
        form.instance.amount = IncomeItem.objects.filter(income_id=self.kwargs['id']).aggregate(Sum('total'))[
            'total__sum']
        f = super().form_valid(form)
        return f




















