# Generated by Django 3.2.6 on 2021-10-28 06:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('keeper', '0042_alter_orderitem_total'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('amount', models.IntegerField()),
                ('payment_method', models.CharField(choices=[('Cash', 'Cash'), ('Transfer', 'Transfer'), ('Card', 'Card')], max_length=200)),
                ('outcat', models.CharField(choices=[('Income', 'Income'), ('Order', 'Order')], max_length=200)),
                ('outlay', models.CharField(max_length=200)),
                ('comment', models.TextField()),
                ('amount_type', models.CharField(max_length=200)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
