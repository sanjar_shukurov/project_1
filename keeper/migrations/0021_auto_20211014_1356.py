# Generated by Django 3.2.6 on 2021-10-14 08:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('keeper', '0020_auto_20210930_1519'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='income',
            name='rate',
        ),
        migrations.AlterField(
            model_name='warehouseproduct',
            name='count',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='warehouseproduct',
            name='warehouse',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.warehouse'),
        ),
    ]
