# Generated by Django 3.2.6 on 2021-09-21 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('keeper', '0002_alter_warehouse_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incomeitem',
            name='currency',
            field=models.CharField(choices=[('UZS', 'UZS'), ('USD', 'USD')], max_length=30),
        ),
        migrations.AlterField(
            model_name='product',
            name='status',
            field=models.CharField(choices=[('In Stock', 'In Stock'), ('Out of Stock', 'Out of Stock')], max_length=30),
        ),
    ]
