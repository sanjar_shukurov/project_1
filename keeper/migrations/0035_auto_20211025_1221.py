# Generated by Django 3.2.6 on 2021-10-25 07:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('keeper', '0034_remove_order_cart'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='created_at',
            new_name='created',
        ),
        migrations.RemoveField(
            model_name='order',
            name='mobile',
        ),
        migrations.RemoveField(
            model_name='order',
            name='order_status',
        ),
        migrations.RemoveField(
            model_name='order',
            name='ordered_by',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_address',
        ),
        migrations.RemoveField(
            model_name='order',
            name='subtotal',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='product',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='quantity',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='rate',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='status',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='user',
        ),
        migrations.AddField(
            model_name='order',
            name='counter_party_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.counterparty'),
        ),
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('Open', 'Open'), ('Closed', 'Closed'), ('Completed', 'Completed'), ('Rejected', 'Rejected')], default='Open', max_length=30),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='order',
            name='warehouse',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='keeper.warehouse'),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='count',
            field=models.FloatField(default=1),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='product_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.product'),
        ),
        migrations.AlterField(
            model_name='order',
            name='total',
            field=models.FloatField(default=0),
        ),
    ]
