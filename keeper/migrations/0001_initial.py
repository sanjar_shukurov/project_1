# Generated by Django 3.2.6 on 2021-09-21 09:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CounterParty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('balance', models.FloatField()),
                ('org_name', models.CharField(max_length=50)),
                ('phone', models.CharField(max_length=20)),
                ('phone_2', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=30)),
                ('inn', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=30)),
                ('sms_live', models.CharField(max_length=200)),
                ('extra_info', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Income',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField()),
                ('total', models.FloatField()),
                ('status', models.BigIntegerField()),
                ('rate', models.FloatField()),
                ('counter_party_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.counterparty')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField()),
                ('user_id', models.IntegerField()),
                ('total', models.FloatField()),
                ('rate', models.FloatField()),
                ('counter_party_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.counterparty')),
            ],
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=50)),
                ('balance', models.FloatField()),
                ('debt', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Workers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('phone', models.CharField(max_length=20)),
                ('created', models.DateTimeField()),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('warehouse', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.warehouse')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=30)),
                ('price', models.FloatField()),
                ('code', models.CharField(max_length=30)),
                ('created', models.DateTimeField()),
                ('status', models.CharField(max_length=30)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.productcategory')),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.FloatField()),
                ('price', models.FloatField()),
                ('status', models.CharField(max_length=40)),
                ('total', models.FloatField()),
                ('rate', models.FloatField()),
                ('order_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.order')),
                ('product_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.product')),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='warehouse',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.warehouse'),
        ),
        migrations.CreateModel(
            name='IncomeItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.FloatField()),
                ('total_usd', models.FloatField()),
                ('total_uzs', models.FloatField()),
                ('price', models.FloatField()),
                ('currency', models.CharField(max_length=30)),
                ('income_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.income')),
                ('product_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.product')),
            ],
        ),
        migrations.AddField(
            model_name='income',
            name='warehouse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='keeper.warehouse'),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='warehouse',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.warehouse'),
        ),
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_number', models.CharField(max_length=20)),
                ('driver_name', models.CharField(max_length=40)),
                ('driver_phone', models.CharField(max_length=20)),
                ('warehouse', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='keeper.warehouse')),
            ],
        ),
    ]
