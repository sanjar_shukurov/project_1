# Generated by Django 3.2.6 on 2021-10-19 09:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('keeper', '0027_alter_income_total'),
    ]

    operations = [
        migrations.AlterField(
            model_name='income',
            name='total',
            field=models.FloatField(default=0),
        ),
    ]
