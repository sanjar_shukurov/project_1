# Generated by Django 3.2.6 on 2021-10-18 06:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('keeper', '0023_alter_income_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incomeitem',
            name='count',
            field=models.FloatField(default=1),
        ),
    ]
