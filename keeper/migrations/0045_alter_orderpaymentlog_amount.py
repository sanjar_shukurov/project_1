# Generated by Django 3.2.6 on 2021-11-01 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('keeper', '0044_auto_20211029_1206'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderpaymentlog',
            name='amount',
            field=models.FloatField(),
        ),
    ]
