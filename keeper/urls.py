from django.urls import path
from .views import *

urlpatterns = [
    path('', WarehouseView.as_view(), name='home'),
    path('party/', AgentView.as_view(), name='agent'),
    path('worker/', WorkerView.as_view(), name='worker'),
    path('product/', ProductView.as_view(), name='product'),
    path('wpro/<int:pk>/', WarProductsView.as_view(), name='wpro'),
    path("prodadd", WarProdAddView.as_view(), name="prod-add"),
    path("product-detail/<int:pk>", ProdDetailView.as_view(), name="prod-detail"),
    path("prod-create", ProdAddView.as_view(), name="prod-create"),
    path("counterparty", CounterPartyView.as_view(), name="counterparty-view"),
    path("cparty-detail/<int:pk>", CounterPartyDetail.as_view(), name="cp_detail"),
    path("cparty-create", CounterPartyCreateView.as_view(), name="cp-create"),
    path("income", IncomeView.as_view(), name="income"),
    path("income_detail/<int:pk>", IncomeDetailView.as_view(), name="income-detail"),
    path("income_create", IncomeCreateView.as_view(), name="income-create"),
    path("income_item_create/<int:id>", IncomeItemCreateView.as_view(), name="income-item-create"),
    path("change-status/<int:id>", ChangeStatusView.as_view(), name="change_status"),
    path("order", OrderView.as_view(), name="order"),
    path("order_detail/<int:pk>", OrderDetailView.as_view(), name="order-detail"),
    path("order_create", OrderCreateView.as_view(), name="order-create"),
    path("order_item_create/<int:id>", OrderItemCreateView.as_view(), name="order-item-create"),
    path("change-order-status/<int:id>", ChangeOrderStatusView.as_view(), name="change_order_status"),
    path("payment_view", PaymentView.as_view(), name="payment"),
    path("payment_create/<int:id> ", PaymentCreateView.as_view(), name="payment-create"),
    path("payment-detail/<int:pk>", PaymentDetailView.as_view(), name="payment_detail"),
    path("pay_det/<int:pk>", IncPayDetailView.as_view(), name="inc-pay-detail"),
    path("inc_pay_create/<int:id>", IncPayCreateView.as_view(), name="inc-pay-create")
]
