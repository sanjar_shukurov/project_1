from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import reverse
from django.conf import settings


class Warehouse(models.Model):

    STATUS = (
        ('Empty', 'Empty'),
        ('Full', 'Full'),
    )
    name = models.CharField(max_length=50)
    status = models.CharField(max_length=50, choices=STATUS)
    balance = models.FloatField()
    debt = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class CounterParty(models.Model):
    name = models.CharField(max_length=20)
    balance = models.FloatField()
    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, null=True)
    org_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    phone_2 = models.CharField(max_length=20)
    address = models.CharField(max_length=30)
    inn = models.CharField(max_length=50)
    status = models.CharField(max_length=30)
    sms_live = models.CharField(max_length=200)
    extra_info = models.TextField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cp_detail', kwargs={'pk': self.pk})


class ProductCategory(models.Model):
    Category = (
        ('Art', 'Art'),
        ('Education', 'Education'),
        ('Music', 'Music'),
        ('Fruits', 'Fruits'),
        ('Vegetables', 'Vegetables'),
        ('Furniture', 'Furiture'),
        ('Electronics', 'Electronics'),
        ('Household Chemicals', 'Household Chemicals'),

    )
    title = models.CharField(max_length=40, choices=Category)

    def __str__(self):
        return self.title


class Product(models.Model):
    STATUS = (
        ('In Stock', 'In Stock'),
        ('Out of Stock', 'Out of Stock'),
    )
    title = models.CharField(max_length=30)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE, blank=True, null=True)
    selling_price = models.FloatField()


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('prod-detail', kwargs={'pk': self.pk})


class Income(models.Model):
    STATUS = (
        ('Open', 'Open'),
        ('Closed', 'Closed'),
        ('Completed', 'Completed'),
        ('Rejected', 'Rejected')
    )
    created = models.DateTimeField(auto_now_add=True)
    total = models.FloatField(default=0)
    status = models.CharField(max_length=30, choices=STATUS, default='Open')
    counter_party_id = models.ForeignKey(CounterParty, on_delete=models.SET_NULL, null=True)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


    def get_absolute_url(self):
        return reverse('income')


class IncomeItem(models.Model):
    Choices = (
        ('UZS', 'UZS'),
        ('USD', 'USD'),
    )
    product_id = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    count = models.FloatField(default=1)
    total = models.FloatField()
    price = models.FloatField()
    income_id = models.ForeignKey(Income, on_delete=models.CASCADE, null=True)


class WarehouseProduct(models.Model):
    product = models.ForeignKey(Product, related_name='titles', on_delete=models.SET_NULL, null=True)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, null=True, blank=True)
    count = models.IntegerField(default=1)
    price = models.FloatField(default=0)

    def __str__(self):
        return f'{self.product}'

    def get_absolute_url(self):
        return reverse('prod-detail', kwargs={'pk': self.pk})


class Workers(models.Model):
    name = models.CharField(max_length=30)
    phone = models.CharField(max_length=20)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class Car(models.Model):
    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, null=True)
    car_number = models.CharField(max_length=20)
    driver_name = models.CharField(max_length=40)
    driver_phone = models.CharField(max_length=20)

    def __str__(self):
        return self.car_number


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)

    def __str__(self):
        return self.full_name


class Order(models.Model):
    STATUS = (
        ('Open', 'Open'),
        ('Closed', 'Closed'),
        ('Completed', 'Completed'),
        ('Rejected', 'Rejected')
    )
    created = models.DateTimeField(auto_now_add=True)
    total = models.FloatField(default=0)
    status = models.CharField(max_length=30, choices=STATUS, default='Open')
    counter_party_id = models.ForeignKey(CounterParty, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "Order: " + str(self.id)

    def get_absolute_url(self):
        return reverse('order')


class OrderItem(models.Model):
    product_id = models.ForeignKey(WarehouseProduct, on_delete=models.SET_NULL, null=True)
    count = models.FloatField(default=1)
    total = models.FloatField(default=0)
    price = models.FloatField(default=0, null=True)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.SET_NULL, blank=True, null=True)
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)


class OrderPaymentLog(models.Model):
    Method = (
        ('Cash', 'Cash'),
        ('Transfer', 'Transfer'),
        ('Card', 'Card')
    )
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    payment_method = models.CharField(choices=Method, max_length=200)
    outcat = models.CharField(default="Order", max_length=200)
    outlay = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    comment = models.TextField()
    amount_type = models.CharField(max_length=200)


class IncomePaymentLog(models.Model):
    Method = (
        ('Cash', 'Cash'),
        ('Transfer', 'Transfer'),
        ('Card', 'Card')
    )
    AMT_TYPE = (
        ('Income', 'Income'),
        ('Expense', 'Expense')
    )
    created = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    payment_method = models.CharField(choices=Method, max_length=200)
    outcat = models.CharField(default="Income", max_length=200)
    outlay = models.ForeignKey(Income, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    comment = models.TextField()
    amount_type = models.CharField(choices=AMT_TYPE, max_length=200)