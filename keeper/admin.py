from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Warehouse)
admin.site.register(CounterParty)
admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(Workers)
admin.site.register(WarehouseProduct)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(OrderPaymentLog)
admin.site.register(Income)
admin.site.register(IncomeItem)
admin.site.register(IncomePaymentLog)
